#include<bits/stdc++.h>1
#include<stdlib.h>
#include<stdio.h>
#include "Cell.h"

/*
 * establecer el tiempo de simulacion en sim_time(ms), este programa imprime
 * exactamente la ultima gráfica, para ver el comportamiento  a lo largo del
 * tiempo modificar sim_time
 *
 */

using namespace std;
template <class T>
int toInt(const T &x){stringstream s;s << x;int r;s >> r;return r;}

int main(int argc, char** argv){
    unsigned long long int  nrepeat;     // numero de ciclos
    db tbegin;      // tiempo de inicio del primer estímulo
    db BCL;         // frecuencia de excitacion en mse
    db CI;          // intervalo de acoplamiento para el ultimo
    db dt;          // paso de tiempo
    db dtstim;      // duracion del estimulo
    db CurrStim;    // corriente de estimulo
    db cell_type;   // tipo de celula
    db ICCT;        // condiciones iniciales para el tipo de celula
    int nstp_prn;    // frecuencia con la que se imprimen los resultados
    long double t=0.0;
    db tend;
    db flag_stm=1.0;
    db Istim=0.0;
    unsigned long long int nstp;
    db cont_repeat = 0.0;
    db Iion;
    int sim_time;
    //int sim_time =  60;  //segundos
    unsigned long long time_to_print;
    if (argc > 1) {
        sim_time = toInt(argv[1]);
        if(sim_time==0)sim_time=1;
    }else{
        printf("no recibí parametro de segundos a simular");
        return 0;
    }

    ///////////////////////////////
    // set values for testing
    ///////////////////////////////
    tbegin = 0.0;
    BCL = 600.0;
    CI = 0;
    dt = 0.02;
    dtstim = 2.0;
    CurrStim = -2000;
    cell_type = 1.0;
    ICCT = 1.0;
    nrepeat = ceil(sim_time*1000/BCL);

    tend = tbegin+dtstim;
    nstp = (tbegin+BCL*nrepeat+CI)/dt;
    time_to_print = nstp-(BCL/dt);

    Cell c;          // instancia de una celula;

    nstp_prn = 10;

    for(unsigned long long int i=0; i<=nstp; ++i,t+=dt){
        if(t>=tbegin && t<=tend){    //esto se cumple entre t:[50,52]
            Istim = CurrStim;
            flag_stm = 0.0;
        }
        else{
            Istim = 0.0;
            if(flag_stm == 0.0){
                if (cont_repeat < nrepeat){
                    tbegin=tbegin+BCL;
                }
                else if (cont_repeat == nrepeat){
                    tbegin=tbegin+CI;
                }
                cont_repeat = cont_repeat +1;
                tend=tbegin+dtstim;
                flag_stm=1.0;
            }
        }

        Iion = c.getItot(dt);
        c.V = c.V - dt*(Iion+Istim)*(1.0/c.Cap);

        //print state each x time
        if(i%nstp_prn==0 && i>=time_to_print){
            printf("%Lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",t,c.V,c.INa,c.IK1,c.Ito,c.IKur,c.IKr,c.IKs,c.ICal,c.IpCa,c.INaK,c.INaca,c.IbNa,c.IbCa,c.Cai,c.Ca_up,c.Ca_rel);
         }
    }
    return 0;
}
