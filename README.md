## Atrium Model

## Table of Content

[Description](#description)

[Dependencies](#dependencies)

[One cell simulation](#one-cell-simulation)

-[Compiling](#compiling-one-cell)

-[Running](#running-one-cell)

-[Testing cell](#testing-cell)

[strand and tissue simulation](#strand-and-tissue-simulation)

-[Compiling](#compiling)

-[Running](#running)

[Plotting](#plotting)

[Contributors](#contributors)


## Description
This proyect simulate the action potential propagation in a very
small region of atrium, we use [the Courtemanche-Ramirez-Natel
Model for Human Atrial Cells](http://ajpheart.physiology.org/content/275/1/H301.long)
and the Crank-Nicholson numeric method for solve the resulting PDE.
we use the monodomain equation for simulating a tissue and strand of myocytes
in a CPU (secuelcial solution) and GPU (parallel solution with CUDA)

## Dependencies
* [Armadillo](http://arma.sourceforge.net/) a C++ linear algebra library
* [CUDA](http://www.nvidia.com/object/cuda_home_new.html) parallel computing platform
* [matplotlib](http://matplotlib.org/) library for save plots (Optional)
* [GNU Octave](https://gnu.org/software/octave/) library for plot output data (optional)
* GNU/linux operative system (because it's fun)

## One Cell Simulation
simulation of one cell can be found in **simulationCell.cc**

### Compiling one cell
go to *src* dir and just run:

```bash
make cell
```

### Running one cell
**simulationCell.cc** receive an int *s* (s>0) as parameter where *s* is the time of simulation
in seconds so you can run this in *bin* dir like this:

```bash
./cell 60 > path_to_output_file
```

### testing cell
there are a second way for run simulation of one cell that exec and plot output
data during 10 minutes ploting data each minute, so in *src* dir:

```bash
./testing.sh
```
feel free to modify these script



## Strand and tissue simulation

### Compiling
* for strand (1D)

in *src* dir:

``` bash
make 1D_CN
```

* for Tissue (2D)

``` bash
make 2D_CN
```

### Running

the logic is the same, in *src* dir:

* for 1D
``` bash
./run.sh 1D_CN
```

* for 2D
``` bash
make 2D_CN
```




## Plotting
for ploting data you can use [GNU Octave](https://gnu.org/software/octave/)
like this:

first go to dir of output file to plot:

``` bash
octave --no-gui
load("out")
plot(out(:,1),out(:,[2:20]))
```

keep in mind that in output files first column is time.
in cases of one cell second column is V, but in 1D and 2D columns greater than one are
V of i cell

## Contributors
* [kala855](https://bitbucket.org/kala855/)
* [danielfms](https://bitbucket.org/danielfms/)
* [choff](https://bitbucket.org/ivanchoff/)
